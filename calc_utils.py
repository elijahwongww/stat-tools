import math
import traceback
import numpy as np

'''
Function: calc_stat
Description: calculate the statistics of the confusion matrix
Input: confusion_matrix_list: a list of confusion matrix
matrix = [tp, tn, fp, fn]
Output: stat_list: a list of statistical metrics

'''
def calc_stat(confusion_matrix_list,name_string):


	stat_list={
		"obj":name_string,
		"accuracy":"",
		"precision":"",
		"negative predictive value":"",
		"recall rate":"",
		"specificity":"",
		"MCC":"",
		"f1-score":"",
		"tp": confusion_matrix_list[0],
		"tn": confusion_matrix_list[1],
		"fp": confusion_matrix_list[2],
		"fn": confusion_matrix_list[3],
	}
	if sum(value for value in confusion_matrix_list)!=0:
		stat_list["accuracy"]=(confusion_matrix_list[0]+confusion_matrix_list[1])/(sum(value for value in confusion_matrix_list))
	else:
		stat_list["accuracy"] = 'ZeroDivisionError'
	if (confusion_matrix_list[0]+confusion_matrix_list[2])!=0:
		stat_list["precision"]=(confusion_matrix_list[0]/(confusion_matrix_list[0]+confusion_matrix_list[2]))
	else:
		stat_list['precision']='ZeroDivisionError'
	if (confusion_matrix_list[1]+confusion_matrix_list[3])!=0:
		stat_list["negative predictive value"]=(confusion_matrix_list[1]/(confusion_matrix_list[1]+confusion_matrix_list[3]))
	else:
		stat_list['negative predictive value']='ZeroDivisionError'
	if (confusion_matrix_list[0]+confusion_matrix_list[3])!=0:
		stat_list["recall rate"]=(confusion_matrix_list[0]/(confusion_matrix_list[0]+confusion_matrix_list[3]))
	else:
		stat_list["recall rate"]='ZeroDivisionError'
	if (confusion_matrix_list[1]+confusion_matrix_list[2])!=0:
		stat_list["specificity"]=(confusion_matrix_list[1]/(confusion_matrix_list[1]+confusion_matrix_list[2]))
	else:
		stat_list["specificity"]='ZeroDivisionError'
	if ((confusion_matrix_list[0]+confusion_matrix_list[2])*(confusion_matrix_list[0]+confusion_matrix_list[3])*(confusion_matrix_list[1]+confusion_matrix_list[2])*(confusion_matrix_list[1]+confusion_matrix_list[3])!=0):
		try:
			stat_list["MCC"]=((confusion_matrix_list[0]*confusion_matrix_list[1])-(confusion_matrix_list[2]*confusion_matrix_list[3]))/(math.sqrt((confusion_matrix_list[0]+confusion_matrix_list[2])*(confusion_matrix_list[0]+confusion_matrix_list[3])*(confusion_matrix_list[1]+confusion_matrix_list[2])*(confusion_matrix_list[1]+confusion_matrix_list[3])))
		except Exception as error:
			print(error.__class__.__name__)
			print(traceback.format_exc())
			stat_list["MCC"]=error.__class__.__name__
	else:
		stat_list["MCC"]=np.nan
	try:
		stat_list["f1-score"]=2*confusion_matrix_list[0]/(2*confusion_matrix_list[0]+confusion_matrix_list[2]+confusion_matrix_list[3])
	except Exception as error:
		print(error.__class__.__name__)
		print(traceback.format_exc())
		stat_list["f1-score"]=error.__class__.__name__
	return stat_list
